package de.tudresden.inf.st.ros2rag.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * Data class for initial configuration.
 *
 * @author rschoene - Initial contribution
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataConfiguration {
  public ActualConfiguration panda_mqtt_connector;
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ActualConfiguration {
    public String server = "tcp://localhost:1883";
    public DataTopics topics;
    public int zone_size;
    public double robot_speed_factor;
    public String robot_planning_mode = "fluid_path";
    public String default_trajectory = "<none>";
    public List<String> zones;
    public Map<String, SortedMap<String, String>> parts;
    public Map<String, SortedMap<String, String>> end_effectors;
    public List<DataWorkPose> goal_poses;
  }
  public static class DataTopics {
    public String robotConfig;
    public String trajectory;
    public String nextStep;
  }
  public static class DataWorkPose {
    public String position;
    public String orientation;
    public String work;
  }
}
