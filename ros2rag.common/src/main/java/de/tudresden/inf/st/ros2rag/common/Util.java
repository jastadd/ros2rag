package de.tudresden.inf.st.ros2rag.common;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLParser;
import de.tudresden.inf.st.ros2rag.common.DataConfiguration.ActualConfiguration;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.SortedMap;

/**
 * Helper method dealing with config.
 *
 * @author rschoene - Initial contribution
 */
public class Util {

  public static ActualConfiguration parseConfig(File configFile) throws IOException {
    System.out.println("Using config file: " + configFile.getAbsolutePath());
    ObjectMapper mapper = new ObjectMapper(
        new YAMLFactory().configure(JsonParser.Feature.ALLOW_YAML_COMMENTS, true)
    );
    ActualConfiguration config = mapper.readValue(configFile, DataConfiguration.class).panda_mqtt_connector;
    URI serverUri = URI.create(config.server);
    config.server = serverUri.getHost() + ":" + (serverUri.getPort() == -1 ? 1883 : serverUri.getPort())
        + serverUri.getPath();
    return config;
  }

  public static void setMqttHost(SetHost handler, ActualConfiguration config) throws IOException {
    HostAndPort hostAndPort = split(config.server);
    handler.setHost(hostAndPort.host, hostAndPort.port);
  }

  public static String mqttUri(String topic, ActualConfiguration config) {
    return "mqtt://" + config.server + "/" + topic;
  }

  public static void iterateLinks(HandleLink callback, ActualConfiguration config) throws IOException {
    for (Map.Entry<String, SortedMap<String, String>> dataRobot : config.parts.entrySet()) {
      String topicPrefix = dataRobot.getKey() + "/";
      for (Map.Entry<String, String> dataLink : dataRobot.getValue().entrySet()) {
        String name = dataLink.getKey();
        callback.handle(false, topicPrefix + name, name);
      }
    }
    for (Map.Entry<String, SortedMap<String, String>> dataRobot : config.end_effectors.entrySet()) {
      String topicPrefix = dataRobot.getKey() + "/";
      for (Map.Entry<String, String> dataLink : dataRobot.getValue().entrySet()) {
        String name = dataLink.getKey();
        callback.handle(true, topicPrefix + name, name);
      }
    }
  }

  private static HostAndPort split(String serverString) {
    HostAndPort result = new HostAndPort();
    String[] serverTokens = serverString.replace("tcp://", "").split(":");
    result.host = serverTokens[0];
    result.port = Integer.parseInt(serverTokens[1]);
    return result;
  }

  private static class HostAndPort {
    String host;
    int port;
  }

  @FunctionalInterface
  public interface SetHost {
    void setHost(String host, int port) throws IOException;
  }

  @FunctionalInterface
  public interface HandleLink {
    void handle(boolean isEndEffector, String topic, String name) throws IOException;
  }
}
