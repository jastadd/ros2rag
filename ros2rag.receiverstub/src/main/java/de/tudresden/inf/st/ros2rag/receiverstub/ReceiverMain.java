package de.tudresden.inf.st.ros2rag.receiverstub;

import com.google.protobuf.InvalidProtocolBufferException;
import config.Config.RobotConfig;
import de.tudresden.inf.st.ros2rag.common.DataConfiguration.ActualConfiguration;
import de.tudresden.inf.st.ros2rag.common.Util;
import de.tudresden.inf.st.ros2rag.receiverstub.ast.MqttHandler;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import plan.TrajectoryOuterClass.Trajectory;
import robot.RobotStateOuterClass.RobotState;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ReceiverMain {

  private static final Logger logger = LogManager.getLogger(ReceiverMain.class);
  private String topicPattern;

  public static void main(String[] args) throws Exception {
    ReceiverMain main = new ReceiverMain();
    File configFile = new File(args[0]);
    ActualConfiguration config = Util.parseConfig(configFile);
    main.run(config);
  }

  private void run(ActualConfiguration config) throws IOException, InterruptedException {
    final CountDownLatch finish = new CountDownLatch(1);

    AtomicInteger topicMaxLength = new AtomicInteger();
    MqttHandler receiver = new MqttHandler("receiver stub");
    Util.setMqttHost(receiver::setHost, config);
    receiver.waitUntilReady(2, TimeUnit.SECONDS);
    receiver.newConnection(config.topics.robotConfig, this::printRobotConfig);
    receiver.newConnection(config.topics.trajectory, this::printTrajectory);
    receiver.newConnection(config.topics.nextStep, this::printNextStep);

    Util.iterateLinks((isEndEffector, topic, name) -> {
      receiver.newConnection(topic, this::printPandaLinkState);
      topicMaxLength.set(Math.max(topicMaxLength.get(), topic.length()));
    }, config);
    this.topicPattern = "%" + topicMaxLength + "s";

    receiver.newConnection("components", bytes -> {
      String message = new String(bytes);
      logger.info("Components: {}", message);
    });

    receiver.newConnection("receiver", bytes -> {
      String message = new String(bytes);
      if (message.equals("exit")) {
        finish.countDown();
      }
    });

    Runtime.getRuntime().addShutdownHook(new Thread(receiver::close));
    finish.await();
    receiver.close();
  }

  private void printPandaLinkState(byte[] bytes) {
    try {
      logger.debug("Got a joint message, parsing ...");
      RobotState pls = RobotState.parseFrom(bytes);
      RobotState.Position position = pls.getPosition();
      RobotState.Orientation orientation = pls.getOrientation();
      RobotState.LinearTwist linearTwist = pls.getLinearTwist();
      RobotState.AngularTwist angularTwist = pls.getAngularTwist();
      // panda::panda_link0: pos(-3.0621333E-8,-1.5197388E-8,3.3411725E-5), orient(0.0,0.0,0.0,0.0), twist-linear(0.0,0.0,0.0), twist-angular(0.0,0.0,0.0)

      logger.printf(Level.INFO,topicPattern +
              ": pos(% .5f,% .5f,% .5f), ori(%.1f,%.1f,%.1f,%.1f)," +
              " twL(%.1f,%.1f,%.1f), twA(%.1f,%.1f,%.1f)",
          pls.getName(),
          position.getX(), position.getY(), position.getZ(),
          orientation.getX(), orientation.getY(), orientation.getZ(), orientation.getW(),
          linearTwist.getX(), linearTwist.getY(), linearTwist.getZ(),
          angularTwist.getX(), angularTwist.getY(), angularTwist.getZ());
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
  }

  private void printRobotConfig(byte[] bytes) {
    try {
      logger.debug("Got a robotConfig message, parsing ...");
      RobotConfig robotConfig = RobotConfig.parseFrom(bytes);
      logger.info("robotConfig: speed = {}", robotConfig.getSpeed());
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
  }

  private void printTrajectory(byte[] bytes) {
    try {
      logger.debug("Got a trajectory message, parsing ...");
      if (logger.isInfoEnabled()) {
        Trajectory trajectory = Trajectory.parseFrom(bytes);
        StringBuilder sb = new StringBuilder("trajectory: [");
        boolean first = true;
        for (Trajectory.Pose pose : trajectory.getPoseList()) {
          Trajectory.Position position = pose.getPosition();
          Trajectory.Orientation orientation = pose.getOrientation();
          Trajectory.PlanningMode mode = pose.getMode();
          sb.append(String.format(
              "pos(% .5f,% .5f,% .5f), ori(%.1f,%.1f,%.1f,%.1f) plan: %s",
              position.getX(),
              position.getY(),
              position.getZ(),
              orientation.getX(),
              orientation.getY(),
              orientation.getZ(),
              orientation.getW(),
              mode.name()));
          if (first) {
            first = false;
          } else {
            sb.append(" | ");
          }
        }
        sb.append("] loop: ").append(trajectory.getLoop());
        logger.info(sb.toString());
      }
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }
  }

  private void printNextStep(byte[] bytes) {
    logger.info("nextStep: {}",
        java.nio.ByteBuffer.wrap(bytes).getInt());
  }

}
