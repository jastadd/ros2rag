package de.tudresden.inf.st.ros2rag.goal;

import de.tudresden.inf.st.ros2rag.common.DataConfiguration.ActualConfiguration;
import de.tudresden.inf.st.ros2rag.common.DataConfiguration.DataWorkPose;
import de.tudresden.inf.st.ros2rag.common.Util;
import de.tudresden.inf.st.ros2rag.goal.ast.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import plan.TrajectoryOuterClass;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Testing Ros2Rag without generating something.
 *
 * @author rschoene - Initial contribution
 */
public class GoalMain {

  private static final Logger logger = LogManager.getLogger(GoalMain.class);
  private MqttHandler mainHandler;
  private GoalModel model;

  public void run(String[] args) throws IOException, InterruptedException {
    File configFile = new File(args.length == 0 ? "../ros2rag.common/config.yaml" : args[0]);

    // --- No configuration below this line ---

    ActualConfiguration config = Util.parseConfig(configFile);

    model = new GoalModel();

    for (DataWorkPose dataWorkPose : config.goal_poses) {
      WorkPose workPose = new WorkPose();
      String[] position = dataWorkPose.position.split(" ");
      String[] orientation = dataWorkPose.orientation.split(" ");
      if (position.length != 3) {
        logger.error("Found invalid work pose position: {}", Arrays.toString(position));
      }
      if (orientation.length != 4) {
        logger.error("Found invalid work pose orientation: {}", Arrays.toString(orientation));
      }
      workPose.setPositionX(Double.parseDouble(position[0]));
      workPose.setPositionY(Double.parseDouble(position[1]));
      workPose.setPositionZ(Double.parseDouble(position[2]));
      workPose.setOrientationX(Double.parseDouble(orientation[0]));
      workPose.setOrientationY(Double.parseDouble(orientation[1]));
      workPose.setOrientationZ(Double.parseDouble(orientation[2]));
      workPose.setOrientationW(Double.parseDouble(orientation[3]));
      workPose.setDuration(Long.parseLong(dataWorkPose.work));
      model.addWorkPose(workPose);
    }
    RobotState robotState = new RobotState();
    robotState.setCurrentPosition(TrajectoryOuterClass.Trajectory.Position.newBuilder().build());
    robotState.setLastUpdate(0);
    model.setRobotState(robotState);

    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    logger.debug("Setting dependencies");
    /*
    Workflow.ReadyForThisStep canDependOn RobotState.LastUpdate as LastUpdateDependency ;
    Workflow.ReadyForThisStep canDependOn RobotState.CurrentPosition as PositionDependency ;
    Workflow.NextPosition canDependOn Workflow.CurrentStep as OnStepChangeDependency ;
     */
    model.getWorkflow().addLastUpdateDependency(robotState);
    model.getWorkflow().addPositionDependency(robotState);
    model.getWorkflow().addOnStepChangeDependency(model.getWorkflow());

    logger.debug("Connecting to topics");
    /*
    read RobotState.CurrentPosition using ParseLinkState, LinkStateToDoublePosition ;
    read RobotState.LastUpdate using TickWhenLinkState ;
    write Workflow.ReadyForThisStep ;  // |_ Those two roles encode are attribute-driven rewrite (via mqtt)
    read Workflow.CurrentStep ;        // |
    write Workflow.NextPosition using CreateTrajectoryMessage, SerializeTrajectory ;
     */
    Util.iterateLinks((isEndEffector, topic, name) -> {
      if (isEndEffector) {
        robotState.connectCurrentPosition(Util.mqttUri(topic, config));
        robotState.connectLastUpdate(Util.mqttUri(topic, config));
      }
    }, config);
    // next position is not initialized, so don't send it
    model.getWorkflow().connectNextTrajectory(Util.mqttUri(config.topics.trajectory, config), false);
    model.getWorkflow().connectCurrentStep(Util.mqttUri(config.topics.nextStep, config));
    // initial next step is sent, as soon as this is received, the workflow starts
    model.getWorkflow().connectReadyForThisStep(Util.mqttUri(config.topics.nextStep, config), true);

    logStatus("Start");
    CountDownLatch exitCondition = new CountDownLatch(1);

    logger.info("To print the current model states, send a message to the topic 'model'.");
    logger.info("To exit the system cleanly, send a message to the topic 'exit', or use Ctrl+C.");

    mainHandler = new MqttHandler("mainHandler").dontSendWelcomeMessage();
    Util.setMqttHost(mainHandler::setHost, config);
    mainHandler.waitUntilReady(2, TimeUnit.SECONDS);
    mainHandler.newConnection("exit", bytes -> exitCondition.countDown());
    mainHandler.newConnection("model", bytes -> logStatus(new String(bytes)));

//    sendInitialDataConfig(mainHandler, config.topics.dataConfig);

    Runtime.getRuntime().addShutdownHook(new Thread(this::close));

    exitCondition.await();

    this.close();
  }

//  private void sendInitialDataConfig(MqttHandler mainHandler, String dataConfigTopic) {
//    Dataconfig.DataConfig dataConfig = Dataconfig.DataConfig.newBuilder()
//        .setEnablePosition(true)
//        .setEnableOrientation(false)
//        .setEnableTwistAngular(false)
//        .setEnableTwistLinear(false)
//        .setPublishRate(100)
//        .build();
//    mainHandler.publish(dataConfigTopic, dataConfig.toByteArray(), true);
//  }

  private void logStatus(String prefix) {
    StringBuilder sb = new StringBuilder(prefix).append("\n");
    for (Step step : model.getWorkflow().getSteps()) {
      sb.append(" ").append(step.prettyPrint()).append("\n");
    }
    sb.append("CurrentStep: ").append(model.getWorkflow().getCurrentStep())
//        .append(", lastStart: ").append(model.getWorkflow().getCurrentStepStart())
        .append(", readyForThisStep: ").append(model.getWorkflow().getReadyForThisStep()).append("\n");
    sb.append("CurrentPosition: ").append(model.getRobotState().getCurrentPosition())
        .append(", lastUpdate: ").append(model.getRobotState().getLastUpdate()).append("\n");
    logger.info(sb.toString());
  }

  private void close() {
    logger.info("Exiting ...");
    mainHandler.close();
    model.ragconnectCloseConnections();
  }

  public static void main(String[] args) throws IOException, InterruptedException {
    new GoalMain().run(args);
  }
}
