package de.tudresden.inf.st.ros2rag.starter;

import de.tudresden.inf.st.ros2rag.common.DataConfiguration.ActualConfiguration;
import de.tudresden.inf.st.ros2rag.common.Util;
import de.tudresden.inf.st.ros2rag.starter.ast.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Testing Ros2Rag without generating something.
 *
 * @author rschoene - Initial contribution
 */
public class StarterMain {

  private static final Logger logger = LogManager.getLogger(StarterMain.class);
  private MqttHandler mainHandler;
  private Model model;

  public void run(String[] args) throws IOException, InterruptedException {
    File configFile = new File(args.length == 0 ? "../ros2rag.common/config.yaml" : args[0]);

    // --- No configuration below this line ---

    ActualConfiguration config = Util.parseConfig(configFile);
    model = new Model();

    ZoneModel zoneModel = new ZoneModel();

    Zone safetyZone = new Zone();
    for (String zone : config.zones) {
      int[] coordinate = {0, 0, 0};
      String[] zoneSplit = zone.split(" ");
      for (int i = 0; i < zoneSplit.length; i++) {
        coordinate[i] = Integer.parseInt(zoneSplit[i]);
      }
      safetyZone.addCoordinate(new Coordinate(
          IntPosition.of(coordinate[0], coordinate[1], coordinate[2])));
    }
    zoneModel.addSafetyZone(safetyZone);
    model.setZoneModel(zoneModel);
    model.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    RobotArm robotArm = new RobotArm();
    model.setRobotArm(robotArm);

    Util.iterateLinks((isEndEffector, topic, name) -> {
      Link link;
      if (isEndEffector) {
        EndEffector endEffector = new EndEffector();
        robotArm.setEndEffector(endEffector);
        link = endEffector;
      } else {
        link = new Link();
        robotArm.addLink(link);
      }
      link.setName(name);
      link.setCurrentPosition(IntPosition.of(0, 0, 0));
      link.containingRobotArm().addDependency1(link);
      link.connectCurrentPosition(Util.mqttUri(topic, config));
    }, config);

    robotArm.connectAppropriateSpeed(Util.mqttUri(config.topics.robotConfig, config), true);

    logStatus("Start", robotArm);
    CountDownLatch exitCondition = new CountDownLatch(1);

    logger.info("To print the current model states, send a message to the topic 'model'.");
    logger.info("To exit the system cleanly, send a message to the topic 'exit', or use Ctrl+C.");

    mainHandler = new MqttHandler("mainHandler");
    Util.setMqttHost(mainHandler::setHost, config);
    mainHandler.waitUntilReady(2, TimeUnit.SECONDS);
    mainHandler.newConnection("exit", bytes -> exitCondition.countDown());
    mainHandler.newConnection("model", bytes -> logStatus(new String(bytes), robotArm));

    Runtime.getRuntime().addShutdownHook(new Thread(this::close));

    exitCondition.await();

    this.close();
  }

  private void logStatus(String prefix, RobotArm robotArm) {
    StringBuilder sb = new StringBuilder(prefix).append("\n")
        .append("robotArm.isInSafetyZone: ").append(robotArm.isInSafetyZone())
        .append(", robotArm.getAppropriateSpeed = ").append(robotArm.getAppropriateSpeed()).append("\n");
    for (Link joint : robotArm.getLinkList()) {
      sb.append(joint.getName()).append(": ").append(joint.getCurrentPosition()).append("\n");
    }
    sb.append("endEffector ").append(robotArm.getEndEffector().getName()).append(": ")
        .append(robotArm.getEndEffector().getCurrentPosition());
    logger.info(sb.toString());
  }

  private void close() {
    logger.info("Exiting ...");
    mainHandler.close();
    model.ragconnectCloseConnections();
  }

  public static void main(String[] args) throws IOException, InterruptedException {
    new StarterMain().run(args);
  }
}
