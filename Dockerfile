FROM ubuntu:bionic

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /home/user
RUN useradd --create-home --home-dir $HOME user \
    && chmod -R u+rwx $HOME \
    && chown -R user:user $HOME

RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends openjdk-8-jdk \
    && apt-get clean

COPY --chown=user:user . /ros2rag/

USER user
WORKDIR /ros2rag

RUN ./gradlew --no-daemon installDist

ENV TARGET starter
ENV CONFIG_FILE ./ros2rag.common/config.yaml
ENTRYPOINT /bin/bash -c "./ros2rag.${TARGET}/build/install/ros2rag.${TARGET}/bin/ros2rag.${TARGET} ${CONFIG_FILE}"

