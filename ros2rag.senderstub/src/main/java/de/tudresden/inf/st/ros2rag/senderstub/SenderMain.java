package de.tudresden.inf.st.ros2rag.senderstub;

import de.tudresden.inf.st.ros2rag.senderstub.ast.MqttHandler;
import robot.RobotStateOuterClass.RobotState;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class SenderMain {
  public static void main(String[] args) throws Exception {
    // either 3 or 4 arguments
    // 3 arguments: topic filename wait
    // 4 arguments: topic x y z
    // assume 4 arguments
    if (args.length < 3 || args.length > 4) {
      System.err.println("Either sends a new position, arguments: topic x y z");
      System.err.println("Or reads positions from a file, arguments: topic filename wait");
      return;
    }
    MqttHandler sender = new MqttHandler("sender stub").dontSendWelcomeMessage();
    sender.setHost("localhost", 1883);
    sender.waitUntilReady(2, TimeUnit.SECONDS);
    final String topic = args[0];
    if (args.length == 3) {
      final String filename = args[1];
      final int wait = Integer.parseInt(args[2]);
      try {
        for (String line : Files.readAllLines(Paths.get(filename))) {
          if (line.isEmpty()) continue;
          String[] lineSplit = line.split(";");
          publish(sender, topic,
              Float.parseFloat(lineSplit[0]),
              Float.parseFloat(lineSplit[1]),
              Float.parseFloat(lineSplit[2]));
          TimeUnit.MILLISECONDS.sleep(wait);
        }
      } catch (InterruptedException ignore) {
      }
    } else {
      publish(sender, topic,
          Float.parseFloat(args[1]),
          Float.parseFloat(args[2]),
          Float.parseFloat(args[3]));
    }
    sender.close();
  }

  private static void publish(MqttHandler sender, String topic, float x, float y, float z) {
    RobotState pls = RobotState.newBuilder()
        .setName(topic)
        .setPosition(RobotState.Position.newBuilder().setX(x).setY(y).setZ(z).build())
        .build();
    final byte[] message = pls.toByteArray();
    sender.publish(topic, message);
  }
}
